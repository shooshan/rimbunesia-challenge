<?php

class Challenge
{
	private function initData()
	{
		$ch = curl_init("https://rimbunesia.com/api/?data=items"); // such as http://example.com/example.xml
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$data = curl_exec($ch);
		curl_close($ch);

		return json_decode($data);
	}

	private function filtersCategory($data)
	{
		$arr = array();
		foreach($data as $k => $v)
		{
		    if(array_key_exists('category', $v)){
		    	if(!array_key_exists($v->category, $arr)){
		    		$arr[$v->category][] = $this->filtersClub($data, $v->category);
		    	}
		    }
		}

		return $arr;
	}

	private function filtersClub($data, $args)
	{
		$arr = array();
		foreach($data as $k => $v)
		{
		    if(array_key_exists('club', $v) && $v->category == $args){
		        $arr[$v->club][] = $v;
		    }
		}

		return $arr;
	}

	public function processData()
	{
		return $this->filtersCategory($this->initData());
	}
}

header("Content-Type: application/json");

$challenge = new Challenge();
echo json_encode($challenge->processData());
?>